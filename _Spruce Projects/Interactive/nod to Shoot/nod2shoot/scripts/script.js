//==============================================================================
// Welcome to scripting in Spark AR Studio! Helpful links:
//
// Scripting Basics - https://fb.me/spark-scripting-basics
// Reactive Programming - https://fb.me/spark-reactive-programming
// Scripting Object Reference - https://fb.me/spark-scripting-reference
// Changelogs - https://fb.me/spark-changelog
//==============================================================================

// Load in the patches module
const Patches = require('Patches');
const Diagnostics = require('Diagnostics');
const Scene = require('Scene');
const FaceTracking = require('FaceTracking');
const Reactive = require("Reactive");
const FaceGestures = require('FaceGestures');

//Locate the user's ship
const ship = Scene.root.find('player');

const scoreText = Scene.root.find('scoreText');
const scoreNumber = Patches.getScalarValue('score');
const enemyisHit = Patches.getBooleanValue('enemyisHit');
Patches.setBooleanValue("enemyisKilled", enemyisHit);

//const isAnimComplete = Patches.getBooleanValue('isAnimComplete');
//Patches.setBooleanValue('animDone', isAnimComplete);

// Store a reference to a detected face
const face = FaceTracking.face(0);

//==============================================================================
// Hide the plane when winking with either eye
//==============================================================================

// Store references for when the left and right eye are closed
const hasLeftEyeClosed = FaceGestures.hasLeftEyeClosed(face);
const hasRightEyeClosed = FaceGestures.hasRightEyeClosed(face);

// Create a signal that returns true when one of the eye booleans is true
const winking = hasLeftEyeClosed.xor(hasRightEyeClosed);


//Score
scoreText.text = scoreNumber.toString();

// Register a blink event
FaceGestures.onBlink(face).subscribe(function () {

    // Store the last known x-axis position value
    const lastPosX = ship.transform.x.pinLastValue();

    //Diagnostics.watch("newPosX is : ", lastPosX);
    Patches.setScalarValue("lastPosX", lastPosX);

});

