const Scene = require('Scene');
const Reactive = require('Reactive');
const TouchGestures = require('TouchGestures')


var cubes_ctrl = Scene.root.find('cubes_controller')
var planeTracker = Scene.root.find('planeTracker0');


TouchGestures.onTap().subscribe(function(gesture) {
	planeTracker.trackPoint(gesture.location);
});

TouchGestures.onPan(planeTracker).subscribe(function(gesture) {
	planeTracker.trackPoint(gesture.location, gesture.state);
});

TouchGestures.onPinch().subscribe(function(gesture) {
	var lastScaleX = cubes_ctrl.transform.scaleX.lastValue;
	cubes_ctrl.transform.scaleX = Reactive.mul(lastScaleX, gesture.scale);

	var lastScaleY = cubes_ctrl.transform.scaleY.lastValue;
	cubes_ctrl.transform.scaleY = Reactive.mul(lastScaleY, gesture.scale);

	var lastScaleZ = cubes_ctrl.transform.scaleZ.lastValue;
	cubes_ctrl.transform.scaleZ = Reactive.mul(lastScaleZ, gesture.scale);
});

TouchGestures.onRotate(cubes_ctrl).subscribe(function(gesture) {
  var lastRotationY = cubes_ctrl.transform.rotationY.lastValue;
  cubes_ctrl.transform.rotationY = Reactive.add(lastRotationY, Reactive.mul(1, gesture.rotation));
});
