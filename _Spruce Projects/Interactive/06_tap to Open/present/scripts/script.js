const Scene = require('Scene');
const Reactive = require('Reactive');
const TouchGestures = require('TouchGestures')


var box_controller = Scene.root.find('box_ctrl')
var planeTracker = Scene.root.find('planeTracker0');


TouchGestures.onTap().subscribe(function(gesture) {
	planeTracker.trackPoint(gesture.location);
});

TouchGestures.onPan(planeTracker).subscribe(function(gesture) {
	planeTracker.trackPoint(gesture.location, gesture.state);
});

TouchGestures.onPinch().subscribe(function(gesture) {
	var lastScaleX = box_controller.transform.scaleX.lastValue;
	box_controller.transform.scaleX = Reactive.mul(lastScaleX, gesture.scale);

	var lastScaleY = box_controller.transform.scaleY.lastValue;
	box_controller.transform.scaleY = Reactive.mul(lastScaleY, gesture.scale);

	var lastScaleZ = box_controller.transform.scaleZ.lastValue;
	box_controller.transform.scaleZ = Reactive.mul(lastScaleZ, gesture.scale);
});

TouchGestures.onRotate(box_controller).subscribe(function(gesture) {
  var lastRotationY = box_controller.transform.rotationY.lastValue;
  box_controller.transform.rotationY = Reactive.add(lastRotationY, Reactive.mul(1, gesture.rotation));
});
