// //==============================================================================
// // Welcome to scripting in Spark AR Studio! Helpful links:
// //
// // Scripting Basics - https://fb.me/spark-scripting-basics
// // Reactive Programming - https://fb.me/spark-reactive-programming
// // Scripting Object Reference - https://fb.me/spark-scripting-reference
// // Changelogs - https://fb.me/spark-changelog
// //==============================================================================

// // How to load in modules
// const Diagnostics = require('Diagnostics');
// const Scene = require('Scene');
// var Textures = require('Textures');
// var Materials = require('Materials');
// var FaceTracking = require('FaceTracking');
// var Animation = require('Animation');
// var Reactive = require('Reactive');
// var TouchGestures = require('TouchGestures');
// const Instruction = require('Instruction');
// var face = FaceTracking.face(0);
// var cuello = Scene.root.find('Bone.004');
// var movimientocuello = 80;
// cuello.transform.rotationX = face.cameraTransform.rotationX.mul(-1.0).sum(0).expSmooth(movimientocuello);
// cuello.transform.rotationY = face.cameraTransform.rotationZ.mul(1.0).sum(0).expSmooth(movimientocuello);
// cuello.transform.rotationZ = face.cameraTransform.rotationY.mul(-1.0).sum(0).expSmooth(movimientocuello);

// // How to access scene objects (uncomment line below to activate)
// // const directionalLight = Scene.root.find('directionalLight0');

// // How to access class properties (uncomment line below to activate)
// // const directionalLightIntensity = directionalLight.intensity;

// // How to log messages to the console (uncomment line below to activate)
// // Diagnostics.log('Console message logged from the script.');
